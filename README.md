# Bypass Paywalls Clean filters

Adblocker list which allows you to read articles from (supported) sites that implement a paywall.\
For some sites it will log you out (or block you to log in); caused by removing cookies or blocking general paywall-scripts.

Disclaimer: the list doesn't support as many sites as the extension/add-on does though (and even less on iOS).\
On iOS you can also use [Shortcuts](https://apps.apple.com/us/app/shortcuts/id915249334) app with [Unpaywall](https://www.icloud.com/shortcuts/71648f5ad34f4d8f972718e5f3621ffe) shortcut for some unsupported sites.

### Installation

#### adblocker filters

Install an adblocker and subscribe to this custom (content)filter: [Bypass Paywalls Clean filters](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/bpc-paywall-filter.txt)

- Chrome/Edge/Firefox: 
- - uBlock Origin [Edge/Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
- - Adguard [Edge/Chrome](https://chrome.google.com/webstore/detail/adguard-adblocker/bgnkhhnnamicmpeenaelnjfhikgbkllg?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/adguard-adblocker/)
- Safari: [AdGuard](https://apps.apple.com/nl/app/adguard-adblock-privacy/id1047223162) (iOS*/iPadOS*) or [Adguard for Mac](https://adguard.com/en/adguard-mac/overview.html) (macOS) 
- Android: use [Via Browser](https://play.google.com/store/apps/details?id=mark.via.gp) or [AdGuard Content Blocker](https://play.google.com/store/apps/details?id=com.adguard.android.contentblocker) 
- Brave: has only incorporated the filterlist (many sites need userscript): enable it in [settings](brave://settings/shields/filters)

*On iOS/iPadOS there's no support for scriptlets (for removing cookies, attributes and/or classes).

#### userscripts

For certain sites, in addition to the adblocking filters a userscript is required to bypass the paywall. 

Use an app or a browser add-on/extension to automatically run the required userscripts:
- Chrome/Edge/Firefox: [Tampermonkey - Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) or [Tampermonkey - Firefox](https://addons.mozilla.org/firefox/addon/tampermonkey/)
- Safari: [Userscripts](https://apps.apple.com/us/app/userscripts/id1463298887) (macOS/iOS/iPadOS) or [Hyperweb](https://apps.apple.com/us/app/hyperweb/id1581824571) (iOS/iPadOS) 
- Android: [AdGuard app](https://adguard.com/adguard-android/overview.html) (load as extension)

Install the userscript(s) (you can add multiple):
- [English](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.en.user.js) (main)
- [Dutch](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.nl.user.js)
- [Finnish/Swedish](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.fi.se.user.js)
- [French](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.fr.user.js)
- [German](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.de.user.js)
- [Italian](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.it.user.js)
- [Spanish/Portugese](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.es.pt.user.js)
